import logging
import requests

# get the logger
log = logging.getLogger(__name__)

# quieten the requests library
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.ERROR)

class Index:
    """All the required to manage submission to Solr"""
    def __init__(self, update_url, site, match):
        self.update_url = "%s/%s" % (update_url, 'update?')
        self.site = site
        self.match = match
        log.debug("Site: %s, Index: %s, Match: %s" % (self.site, self.update_url, self.match))
        self.headers = { 'Content-type': 'text/xml; charset=utf-8' }

    def commit(self):
        """Commit the pending updates"""
        msg = '<commit expungeDeletes="true"/>'
        log.debug("Commit message: %s" % msg)
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully committed the changes.")
        else:
            log.error("Something went wrong trying to commit the changes.")
            log.error("\n%s" % resp.text)

    def clean(self):
        """Delete all documents

        If a site is specified, then only the documents of that site will be purged.
        """
        if self.match is not None:
            msg = "<delete><query>%s</query></delete>" % self.match
        else:
            msg = "<delete><query>*:*</query></delete>"
        resp = requests.post(self.update_url, msg, headers=self.headers)

        log.debug("Purge message: %s" % msg)
        if resp.status_code == 200:
            log.debug("Successfully submitted the index delete request.")
        else:
            log.error("Something went wrong trying to submit a request to wipe the index.")
            log.error("\n%s" % resp.text)

    def optimize(self):
        """Optimize the on disk index"""
        msg = '<optimize waitSearcher="false"/>'
        log.debug("Optimize: message: %s" % msg)
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully optimized the index.")
        else:
            log.error("Something went wrong trying to optimize the index.")
            log.error("\n%s" % resp.text)

    def submit(self, doc, document_name):
        """Submit the document for indexing"""
        resp = requests.post(self.update_url, data=doc, headers=self.headers)
        if resp.status_code == 200:
            log.debug("%s successfully submitted for indexing." % document_name)
        else:
            log.error("Submission of %s failed with error %s." % (document_name, resp.status_code))

